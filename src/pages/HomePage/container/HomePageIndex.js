import React from "react"
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles,createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class HomePageIndex extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            text:'Hello world',
            date: new Date()
        };
    }

    handleClick =()=> {
        console.log('click')
        this.setState({
            text:'change to click'
        });
    }


    componentDidMount() {

    }


    render(){

        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="static" color="default">
                    <Toolbar>
                        <Typography variant="h6" color="inherit">
                            Photos
                        </Typography>
                    </Toolbar>
                </AppBar>
            </div>

        )
    }

}

const styles = createStyles({
    root: {
        flexGrow: 1,
    }
});

HomePageIndex.propTypes = {
    classes:PropTypes.object.isRequired
};

export default withStyles(styles)(HomePageIndex)
