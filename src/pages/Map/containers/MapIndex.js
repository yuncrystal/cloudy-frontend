/**
 * Created by YUN on 12/11/18.
 */
import React from 'react';
import MapComponent from '../components/MapComponent';

//import actions
// import {} from '';


class MapIndex extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div>
                <h1>This is the MapIndex container</h1>
                <MapComponent
                    id="myMap"
                    options={{
                        center: { lat: 41.0082, lng: 28.9784 },
                        zoom: 8
                    }}
                    onMapLoad={map => {
                        let marker = new window.google.maps.Marker({
                            position: { lat: 41.0082, lng: 28.9784 },
                            map: map,
                            title: 'Hello Istanbul!'
                        });
                    }}
                />
            </div>
        )
    }
}


export default MapIndex;