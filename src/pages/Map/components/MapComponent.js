/**
 * Created by YUN on 12/11/18.
 */
import React from 'react';
import PropTypes from 'prop-types';
import {withStyles, createStyles} from '@material-ui/core/styles';


class MapComponent extends React.Component {
    constructor(props) {
        super(props);
    }


    onScriptLoad = () =>{
        const map = new window.google.maps.Map(
            document.getElementById(this.props.id),
            this.props.options);
        this.props.onMapLoad(map)
    }


    componentDidMount() {
        if (!window.google) {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.src = `https://maps.google.com/maps/api/js?key=AIzaSyDrxVtm4Ddal9Ou9InxbA7mbgKme41CMSQ`;
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
            // Below is important.
            //We cannot access google.maps until it's finished loading
            s.addEventListener('load', e => {
                this.onScriptLoad()
            })
        } else {

            this.onScriptLoad()
        }
    }


    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.map} id={this.props.id} >
                    <h1>Google Map</h1>
                </div>

            </div>
        )
    }
}



const styles = createStyles({
    root: {
        flexGrow: 1,
    },
    map: {
        height: 400,
        backgroundColor: 'red'
    }
});

MapComponent.propTypes = {
    classes: PropTypes.object.isRequired,
    onMapLoad: PropTypes.func.isRequired,
};

export default withStyles(styles)(MapComponent)