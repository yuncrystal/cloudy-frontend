import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from "react-redux";
import configureStore from './store/configureStore'
import registerServiceWorker from './registerServiceWorker';
import MainRouter from './router/index';
// import './App.css';

const store = configureStore();


const rootElement = document.getElementById("root");
ReactDOM.render(
    <Provider store={store}>
        <MainRouter/>
    </Provider>,
    rootElement
);
registerServiceWorker();