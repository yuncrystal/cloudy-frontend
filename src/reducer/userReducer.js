/**
 * Created by YUN on 24/9/18.
 */

import variables from '../util/variables';

const INITIAL_STATE = {
    user:'',
    token:null,

};

export default (state = INITIAL_STATE,action) => {
    switch (action.type) {
        case variables.USER.LOGIN_USER:
            return INITIAL_STATE;

        default:
            return state;
    }
}