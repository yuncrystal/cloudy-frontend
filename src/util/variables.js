// 用户的相关变量

const variables = {

    //系统相关变量

    SYSTEM: {
        CURRENCY_AU: 'AUD',
        CURRENCY_AU_SYMBOL: '$',
        CURRENCY_CNY: 'CNY',
        CURRENCY_CNY_SYMBOL: '¥',
        SHOPPING_CART_STORAGE: 'myCart',
        SHOPPING_CART_ORDER_STORAGE: 'order',
        ACCESS_TOKEN: '@access_token',
        REFRESH_TOKEN: '@refresh_token',
        NEW_INSTALL: '@new_install',
        USERNAME: '@username',
        PASSWORD: '@password',
        USER_TYPE: '@userType'
    },

    // User相关
    USER: {

        SIGN_UP_USER: 'sign_up_user',
        SIGN_UP_USER_SUCCESS: 'sign_up_user_success',
        SIGN_UP_USER_FAILED: 'sign_up_user_failed',

        FETCH_USER: 'fetch_user',
        FETCH_USER_SUCCESS: 'fetch_user_success',
        FETCH_USER_FAILED: 'fetch_user_failed',

        LOGIN_USER: 'login_user',
        LOGIN_USER_SUCCESS: 'login_user_success',
        LOGIN_USER_FAILED: 'login_user_failed',


        LOGOUT_USER: 'logout_user',
        LOGOUT_USER_SUCCESS: 'logout_user_success',
        LOGOUT_USER_FAILED: 'logout_user_failed',

        FETCH_USER_AUTHORITY_SUCCESS:'fetch_user_authority_success',
        FETCH_USER_AUTHORITY_FAILED:'fetch_user_authority_failed'

    },

    //登录认证
    AUTH: {
        AUTHENTICATED: 'authenticated',
        UNAUTHENTICATED: 'unauthenticated',

    },
    WIDTHDRAW: {
        INFO_CNY: "info_cny",
        INFO_AUD: "info_aud"
    },
    TEMPLATES:"templates"
}

export default variables;





