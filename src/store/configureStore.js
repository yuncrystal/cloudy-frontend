/**
 * Created by YUN on 21/6/17.
 */
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from '../reducer/rootReducer'

function configureStore() {
    // const initialState = Immutable.Map();
    const composeEnhancers =
        typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
                // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
            }) : compose;
    const enhancer = composeEnhancers(
        // applyMiddleware(thunk),
        applyMiddleware(thunk),
        // other store enhancers if any
    );
    return createStore(
        rootReducer,
        // initialState,
        enhancer,
    );
}

export default configureStore
