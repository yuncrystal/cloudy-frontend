import React from 'react';
import Homepage from '../pages/HomePage/container/HomePageIndex';
import MapIndex from '../pages/Map/containers/MapIndex';
import NotFound from "../pages/Error/NotFound";
import {Switch,Route,BrowserRouter as Router,} from 'react-router-dom';

const MainRouter = () =>(
    <Router>
        <Switch>
            {/*<Route path ='/' component={MainRouter}/>*/}
            <Route exact path="/" component={Homepage}/>
            <Route path="/map" component={MapIndex}/>
            <Route component={NotFound}/>

        </Switch>
    </Router>

)

export default MainRouter;
